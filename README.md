# daverona/docker-compose/pypiserver

## Prerequisites

* `pypiserver.example` mapped to `127.0.0.1` &mdash; check [this](https://gitlab.com/daverona/docker-compose/dnsmasq) out
* [Traefik](https://docs.traefik.io/) &mdash; check [this](http://gitlab.com/daverona/docker-compose/traefik) out

## Quick Start

```bash
cp .env.example .env
# edit .env
# if you need user authentication, copy htpasswd file
cp storage/pypiserver/auth/htpasswd.example storage/pypiserver/auth/htpasswd
docker-compose up --detach
```

> `storage/pypiserver/auth/htpasswd` has a user `admin` with password `secret` by default.
> You must remove this account and add yours. To do so, read Usage section.
> If you don't need user authentication, remove `storage/pypiserver/auth/htpasswd` file.

Create or update `.pypirc` file under your home directory:
```
[distutils]
index-servers=
  pypiserver

[pypiserver]
repository:http://pypiserver.example
username:admin
password:secret
```

Create or update `.pip/pip.conf` file under your home directory:

```
[global]
trusted-host=pypiserver.example
```

Let's test the new pypiserver setup.
We simply build a sample package of [requests](https://requests.readthedocs.io/en/master/), upload the package to pypiserver, and install the package from pypiserver:

```bash
# get requests source
git clone https://github.com/psf/requests
cd requests
# build and upload the package
python3 setup.py sdist upload --repository pypiserver
# ensure no requests package is installed
pip3 uninstall --yes requests
# install the package
pip3 install --index-url http://pypiserver.example/simple/ requests
```

## Usage

### Add User

```bash
docker container run --rm daverona/pypiserver htpasswd -n myname
# enter password
```

Append the output to `storage/pypiserver/auth/htpasswd` file.

## References
